package com.example.jaime.pagoclap;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CardViewCiActivity extends AppCompatActivity{

    private String ci;
    private TextView txtCi;
    private Spinner spinnerCi;
    public Context context;
    Button btnOk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardview_ci);
        txtCi = (TextView) findViewById(R.id.txtci);
        btnOk = (Button) findViewById(R.id.btnOk);

        spinnerCi = (Spinner) findViewById(R.id.spinner_ci);
        List<String> nac = new ArrayList<String>();
        nac.add("V");
        nac.add("E");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, nac);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCi.setAdapter(dataAdapter);
    }


    public void Ingresar(View view) {
        ci = spinnerCi.getSelectedItem()+txtCi.getText().toString();
        new AlertDialog.Builder(this)
                .setTitle("Desea censar a")
                .setMessage(ci)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        txtCi.setText(ci);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context,"No se pudo censar a "+ci,Toast.LENGTH_LONG);
                    }
                })
                .show();

    }
}
